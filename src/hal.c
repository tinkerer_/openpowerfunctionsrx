//Copyright (c) 2012, vsluiter <info-at- hackvandedam.nl>
//
//Permission to use, copy, modify, and/or distribute this software for any
//purpose with or without fee is hereby granted, provided that the above
//copyright notice and this permission notice appear in all copies.
//
//THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
//WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
//AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
//INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
//OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
//TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
//OF THIS SOFTWARE.

//#include <avr/io.h>
//#include <avr/interrupt.h>
#include "hal.h"
//#define _NOP() asm("nop")

void EnableClocks(void)
{
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,  ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,  ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,  ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
}

void Configure105usTimer(void)
{
    TIM_TimeBaseInitTypeDef     TIM_TimeBaseStructure;
    //    /*Timer 15, 105us initialisation*/
    TIM_TimeBaseStructure.TIM_ClockDivision     = TIM_CKD_DIV1; //not used
    TIM_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period            = 420;//105us at 8MHz
    TIM_TimeBaseStructure.TIM_Prescaler         = 1;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; //only useful for counter1
    TIM_TimeBaseInit(TIM_105_US, &TIM_TimeBaseStructure);
}

void Configure100msTimer(void)
{
    TIM_TimeBaseInitTypeDef     TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_ClockDivision     = TIM_CKD_DIV1; //not used
    TIM_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up; //TIM6 can only count up
    TIM_TimeBaseStructure.TIM_Period            = 100;//100ms with prescaler 7999;
    TIM_TimeBaseStructure.TIM_Prescaler         = 7999;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; //only useful for counter1
    TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
}

void ConfigureServoTimer(void)
{
    TIM_TimeBaseInitTypeDef     TIM_TimeBaseStructure;
    TIM_OCInitTypeDef           TIM_OCInitStructure;
//    /*Timer 3, Servotimer initialisation*/
    TIM_TimeBaseStructure.TIM_ClockDivision     = TIM_CKD_DIV1; //not used
    TIM_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period            = 40000; //50Hz: 20ms
    TIM_TimeBaseStructure.TIM_Prescaler         = 3;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; //only useful for counter1
    TIM_TimeBaseInit(TIM_SERVO, &TIM_TimeBaseStructure);
//Servo A & B initialisation
    TIM_OCInitStructure.TIM_OCIdleState         = TIM_OCIdleState_Set; //only valid for TIM1;
    TIM_OCInitStructure.TIM_OCMode              = TIM_OCMode_PWM1;     //PWM outputs
    TIM_OCInitStructure.TIM_OutputState         = TIM_OutputState_Enable; //Enable outputs
    TIM_OCInitStructure.TIM_OutputNState        = TIM_OutputNState_Disable; //No inverse outputs
    TIM_OCInitStructure.TIM_Pulse               = SERVO_MULTIPLIER*SERVO_OFFSET; //half way
    TIM_OC1Init(TIM_SERVO,&TIM_OCInitStructure);
    TIM_OC2Init(TIM_SERVO,&TIM_OCInitStructure);
}

void ConfigurePWMTimer(void)
{
    TIM_TimeBaseInitTypeDef     TIM_TimeBaseStructure;
    TIM_OCInitTypeDef           TIM_OCInitStructure;
    //     PWM Timer initialisation
    TIM_TimeBaseStructure.TIM_ClockDivision     = TIM_CKD_DIV1; //not used
    TIM_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period            = 727; //1100Hz @8MHz Clock. Step size: 727/8 = 91
    TIM_TimeBaseStructure.TIM_Prescaler         = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; //only useful for counter1
    TIM_TimeBaseInit(TIM_PWM, &TIM_TimeBaseStructure);
//Output 1 initialisation
    TIM_OCInitStructure.TIM_OCIdleState         = TIM_OCIdleState_Reset; //only valid for TIM1;
    TIM_OCInitStructure.TIM_OCMode              = TIM_OCMode_PWM1;     //PWM outputs
    TIM_OCInitStructure.TIM_OutputState         = TIM_OutputState_Enable; //Enable outputs
    TIM_OCInitStructure.TIM_OutputNState        = TIM_OutputNState_Disable; //No inverse outputs
    TIM_OCInitStructure.TIM_Pulse               = 0; //OFF
    TIM_OCInitStructure.TIM_OCPolarity          = TIM_OCPolarity_High;

    TIM_OC1Init(TIM_PWM,&TIM_OCInitStructure);
    TIM_OC2Init(TIM_PWM,&TIM_OCInitStructure);
    TIM_OC3Init(TIM_PWM,&TIM_OCInitStructure);
    TIM_OC4Init(TIM_PWM,&TIM_OCInitStructure);
}

void InitGPIO(void)
{
    GPIO_InitTypeDef            GPIO_InitStructure;
        /* Configure PB4 and PB5 in AF, open drain (servo outputs) */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_1);

    /* Configure PA15 output pushpull mode (LED) */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Configure PWM outputs */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //not applicable
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL; //external pullup
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource8,  GPIO_AF_2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9,  GPIO_AF_2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_2);

    /* Configure PB6 button in input mode */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //not applicable
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP; // pullup
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    /* Configure PB3 IR sensor in input mode */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //not applicable
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP; // pullup
    GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void SetupExternalInterrupt(uint8_t mode)
{
    /*Init pin interrupt*/
    //MCUCR = (MCUCR & 0xFC) | mode; //Falling edge INT0 generates interrupt; Table 9-2
}

void Setup105usclock(void)
{
//    /*Timer0 CTC mode, overrun at OCR0A, CLK/8*/
//    TCCR0A = _BV(WGM01);
//    TCCR0B = _BV(CS01);        // CTC mode, TOP is OCR0A, CLK = CLKio/8
//    TCNT0  = 0;
//    OCR0A  = 105; //105us
//    TIMSK0 |= _BV(OCIE0A); //Output compare interrupt enabled
}

void IoInit(void)
{
//    DDRA = 0;
//    DDRB = 0;
//    PORTA = 0x00;
//    PORTB = 0xFF;
//    A_PORT  |= (A_C1|A_C2|B_C1|B_C2);
//    DDRA    |= (A_C1|A_C2|B_C1|B_C2);
//    SERVOADDR |= SERVOAPIN;       //make servoa control pin output
//    SERVOBDDR |= SERVOBPIN;       //make servob control pin output
//    BUTTON_DDR  &= ~BUTTONPINMASK;//pullup on button pin
//    BUTTON_PORT |=  BUTTONPINMASK;//pullup on button pin
//    BICOLOR_LED_OUTPUTS;
}

void SetupPWMTimer(void)
{
//    TCCR1A =0; //CTC
//    TCCR1B = (_BV(WGM13) | _BV(WGM12) | PWMCLK);                 //IOclk/1 ->No clock division
//    TCCR1C = 0;
//    ICR1   = 511; //2x 255 -> 16kHz output frequency
//    OCR1A  = 20;  //Value to turn off output A
//    OCR1B  = 20;  //Value to turn off output B
//    TCNT1  = 0;
//    TIMSK1 = _BV(ICIE1);
}
