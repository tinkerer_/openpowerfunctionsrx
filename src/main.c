/**
  ******************************************************************************
  * @file    OpenLegoRC-ARM/main.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    23-March-2012
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"
#include "hal.h"
#include "openpf.h"
#include "eeprom.h"
/** @addtogroup STM32F0_Discovery_Peripheral_Examples
  * @{
  */

/** @addtogroup OpenLegoRC-ARM
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//#define BSRR_VAL        0x0300
#define SERVO_SAME_CHANNEL 1
#define SERVO_NEXT_CHANNEL 0
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

//uint8_t pwmport, servoatrip, servobtrip;
//uint16_t pwma,pwmb;
//volatile uint8_t  ocr1_mask_a,ocr1_mask_b,ocr1_mask_both = 0xFF;
/* Virtual address defined by the user: 0xFFFF value is prohibited */
uint16_t VirtAddVarTab[NB_OF_VAR] = {0x5555};
uint16_t VarDataTab[NB_OF_VAR] = {0};

volatile uint8_t timerflag105us = 0;
volatile uint8_t externalint = 0;
volatile uint16_t repetitiontimer;
typedef struct TimerStruct
{
    uint16_t number_of_delays;
    int      flag;
} TimerStructTypeDef;
TimerStructTypeDef  TimerPwmABrakeThenFloat= {0,RESET},\
                    TimerPwmBBrakeThenFloat= {0,RESET},\
                    TimerServoPfTimeOut    = {0,RESET},\
                    TimerPwmPfTimeOut      = {0,RESET},\
                    TimerSleepTimeOut      = {0,RESET};

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void delay (int32_t);
static void UpdateOutputValues(struct OpenPfRx_output * );
//static void ResetPWMChannel(struct OpenPfRx_channel *);
//static void ResetServoChannel(struct OpenPfRx_channel *);
static void DecreaseTimers(void);
static void DecreaseTimer(TimerStructTypeDef *);
static uint8_t HandleButton(uint8_t);
enum BTNSTATES {BTN_DO_NOTHING, BTN_SHORT_PUSH, BTN_LONG_PUSH};
/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
    /*!< At this stage the microcontroller clock setting is already configured,
      this is done through SystemInit() function which is called from startup
      file (startup_stm32f0xx.s) before to branch to application main.
      To reconfigure the default setting of SystemInit() function, refer to
      system_stm32f0xx.c file
    */

    EXTI_InitTypeDef            EXTI_InitStructure;
    /* variables for power functions */
    uint8_t channelnumber,servo_channel_mode;
    struct OpenPfRx_channel channel_pwm;
    struct OpenPfRx_channel channel_servo;
    uint16_t legochannel[8] = {0,0,0,0,0,0,0,0}; //can be used to store retrieved data


    FLASH_Unlock();
    EE_Init(); //initialize EEPROM emulation

    EnableClocks();
    Configure105usTimer();
    Configure100msTimer();
    ConfigurePWMTimer();
    ConfigureServoTimer();

    TIM_CtrlPWMOutputs(TIM_PWM, ENABLE);
    TIM_ITConfig(TIM_105_US,TIM_IT_Update, ENABLE);
    TIM_ITConfig(TIM6,TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM_105_US, ENABLE);
    TIM_Cmd(TIM_SERVO , ENABLE);
    TIM_Cmd(TIM_PWM , ENABLE);
    TIM_Cmd(TIM6 , ENABLE);

    InitGPIO();


    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource3);
    EXTI_InitStructure.EXTI_Line    = EXTI_Line3;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_Init(&EXTI_InitStructure);

    NVIC_EnableIRQ(EXTI2_3_IRQn);
    NVIC_EnableIRQ(TIM_105_US_IRQn);
    NVIC_EnableIRQ(TIM6_DAC_IRQn);
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, DISABLE);

    //EE_WriteVariable(VirtAddVarTab[0], 0x00);
    EE_ReadVariable(VirtAddVarTab[0], &VarDataTab[0]);
    if(VarDataTab[0] & 0xFF00)
        servo_channel_mode = SERVO_SAME_CHANNEL;
    else
        servo_channel_mode = SERVO_NEXT_CHANNEL;

    OpenPfRx_channel_init(  (struct OpenPfRx_channel *)&channel_pwm, (uint8_t)(VarDataTab[0]& 0x03));
    if(servo_channel_mode == SERVO_SAME_CHANNEL)
        OpenPfRx_channel_init(  (struct OpenPfRx_channel *)&channel_servo,  (uint8_t)(VarDataTab[0]& 0x03));
    else
        OpenPfRx_channel_init(  (struct OpenPfRx_channel *)&channel_servo, ((uint8_t)(VarDataTab[0]) & 0x03)+1);
    //SetupExternalInterrupt(ExternalInterruptFalling);
    //ENABLE_IR_INT;
    //SetupPWMTimer();
    //ENABLE_PWMA_INTERRUPT;
    //ENABLE_PWMB_INTERRUPT;
    //servoatrip = servobtrip = SERVO_OFFSET; //reset servos to center position
    //Setup105usclock();
    OpenPfRx_rx.newdata = 0;
    while (1)
    {
        //static uint16_t red_led_downcounter = 0;
        //uint8_t c_sreg;
        //if(red_led_downcounter == 0)
        //    BICOLOR_GREEN;
        //else
        //    red_led_downcounter--;
        if(TimerSleepTimeOut.number_of_delays == 0)
        {
            GPIO_ResetBits(GPIOA ,GPIO_Pin_15); //turn off LED
            TIM_Cmd(TIM15, DISABLE);
            NVIC_DisableIRQ(TIM15_IRQn);
            PWR_EnterSleepMode(PWR_SLEEPEntry_WFI); //go to sleep, wait for interrupt
            GPIO_SetBits(GPIOA ,GPIO_Pin_15); //Turn on LED
            TIM_Cmd(TIM15, ENABLE);
            NVIC_EnableIRQ(TIM15_IRQn);
        }
        if(repetitiontimer)
        {
            repetitiontimer = 0;
            DecreaseTimers();
            if(TimerPwmPfTimeOut.flag == SET)
            {
                TimerPwmPfTimeOut.flag = RESET;
                TIM_SetCompare1(TIM1,0);
                TIM_SetCompare2(TIM1,0);
                TIM_SetCompare3(TIM1,0);
                TIM_SetCompare4(TIM1,0);
                channel_pwm.A.output_mode = OM_FLOAT;
                channel_pwm.B.output_mode = OM_FLOAT;
                channel_pwm.A.pwmindex = PWM_FLOAT;
                channel_pwm.B.pwmindex = PWM_FLOAT;
            }
            if(TimerServoPfTimeOut.flag == SET)
            {
                TimerServoPfTimeOut.flag = RESET;
                TIM_SetCompare1(TIM3,SERVO_OFFSET*SERVO_MULTIPLIER);
                TIM_SetCompare2(TIM3,SERVO_OFFSET*SERVO_MULTIPLIER);
                channel_servo.A.output_mode = OM_FLOAT;
                channel_servo.B.output_mode = OM_FLOAT;
                channel_servo.A.pwmindex = PWM_FLOAT;
                channel_servo.B.pwmindex = PWM_FLOAT;
            }
            if(TimerPwmABrakeThenFloat.flag == SET)
            {
                TimerPwmABrakeThenFloat.flag = RESET;
                TIM_SetCompare1(TIM1,0);
                TIM_SetCompare2(TIM1,0);
                channel_pwm.A.output_mode = OM_FLOAT;
                channel_pwm.A.pwmindex = PWM_FLOAT;
            }
            if(TimerPwmBBrakeThenFloat.flag == SET)
            {
                TimerPwmBBrakeThenFloat.flag = RESET;
                TIM_SetCompare3(TIM_PWM,0);
                TIM_SetCompare4(TIM_PWM,0);
                channel_pwm.B.output_mode = OM_FLOAT;
                channel_pwm.B.pwmindex = PWM_FLOAT;
            }
            switch(HandleButton(CHECK_BUTTON_PUSHED))
            {
            case BTN_LONG_PUSH:
            {
                if(servo_channel_mode == SERVO_NEXT_CHANNEL)
                {
                    servo_channel_mode = SERVO_SAME_CHANNEL;
                    //eeprom_write_byte(EEPROM_ADDRESS_SERVO_CHANNEL_MODE,SERVO_SAME_CHANNEL);
                    OpenPfRx_channel_init( (struct OpenPfRx_channel *)&channel_servo,channel_pwm.channel_number );    //re-init channel
                }
                else
                {
                    servo_channel_mode = SERVO_NEXT_CHANNEL;
                    //eeprom_write_byte(EEPROM_ADDRESS_SERVO_CHANNEL_MODE,SERVO_NEXT_CHANNEL);
                    OpenPfRx_channel_init( (struct OpenPfRx_channel *)&channel_servo,(channel_pwm.channel_number+1)&0x03 );    //re-init channel
                }
                //ResetServoChannel(&channel_servo);
                TIM_SetCompare1(TIM_SERVO,SERVO_OFFSET*SERVO_MULTIPLIER);
                TIM_SetCompare2(TIM_SERVO,SERVO_OFFSET*SERVO_MULTIPLIER);
                channel_servo.A.output_mode = OM_FLOAT;
                channel_servo.B.output_mode = OM_FLOAT;
                channel_servo.A.pwmindex = PWM_FLOAT;
                channel_servo.B.pwmindex = PWM_FLOAT;
                EE_WriteVariable(VirtAddVarTab[0], (uint16_t)((servo_channel_mode << 8) + channel_pwm.channel_number));
                break;
            }
            case BTN_SHORT_PUSH:
            {
                uint8_t temp_channel;
                temp_channel = (channel_pwm.channel_number+1) & 0x03;                      //calculate new channel number; increase, but mask 'a' bit-> always return to usable code
                OpenPfRx_channel_init( (struct OpenPfRx_channel *)&channel_pwm,   temp_channel );    //re-init channel
                if(servo_channel_mode == SERVO_NEXT_CHANNEL)
                    OpenPfRx_channel_init( (struct OpenPfRx_channel *)&channel_servo,(temp_channel+1)&0x03 );    //re-init channel
                else
                    OpenPfRx_channel_init( (struct OpenPfRx_channel *)&channel_servo,temp_channel);    //re-init channel
                EE_WriteVariable(VirtAddVarTab[0], (uint16_t)((servo_channel_mode << 8) + temp_channel));
                //eeprom_write_byte(EEPROM_ADDRESS_CHANNEL,temp_channel);                           //write eeprom byte
                TIM_SetCompare1(TIM_PWM,0);
                TIM_SetCompare2(TIM_PWM,0);
                TIM_SetCompare3(TIM_PWM,0);
                TIM_SetCompare4(TIM_PWM,0);
                channel_pwm.A.output_mode = OM_FLOAT;
                channel_pwm.B.output_mode = OM_FLOAT;
                channel_pwm.A.pwmindex = PWM_FLOAT;
                channel_pwm.B.pwmindex = PWM_FLOAT;

                TIM_SetCompare1(TIM_SERVO,SERVO_OFFSET*SERVO_MULTIPLIER);
                TIM_SetCompare2(TIM_SERVO,SERVO_OFFSET*SERVO_MULTIPLIER);
                channel_servo.A.output_mode = OM_FLOAT;
                channel_servo.B.output_mode = OM_FLOAT;
                channel_servo.A.pwmindex = PWM_FLOAT;
                channel_servo.B.pwmindex = PWM_FLOAT;
                break;
            }
            case BTN_DO_NOTHING:
            default:
            {
                break;
            }
            }
        }
        if(OpenPfRx_rx.newdata)
        {
            OpenPfRx_rx.newdata = 0;
            if(OpenPfRxVerifyChecksum(OpenPfRx_rx.rxdata))                         //if Checksum is OK continue processing data
            {
                //BICOLOR_RED;
                //red_led_downcounter = 10000;
                channelnumber = OpenPfRxGetChannelNumber(OpenPfRx_rx.rxdata);      //Read channelnumber from received data
                legochannel[channelnumber] = OpenPfRx_rx.rxdata;
                if(channelnumber == channel_pwm.channel_number)                 //if data is for own channel
                {
                    channel_pwm.timeout = channel_pwm.timeout_limit;                       //reset timeout
                    OpenPfRxInterpreter((const uint16_t *)&legochannel[channelnumber] , &channel_pwm);
                    UpdateOutputValues(&channel_pwm.A); //Calculate new values for output A, C1 and C2
                    UpdateOutputValues(&channel_pwm.B); //Calculate new values for output B, C1 and C2
                    switch(channel_pwm.A.output_mode)
                    {
                    case OM_FWD:
                    {
                        TIM_SetCompare1(TIM1,((uint8_t)(channel_pwm.A.pwmindex)) * 91);
                        TIM_SetCompare2(TIM1,0);
                        TimerPwmABrakeThenFloat.number_of_delays = 0; //no timeout
                        break;
                    }
                    case OM_BWD:
                    {
                        TIM_SetCompare1(TIM1,0);
                        TIM_SetCompare2(TIM1,((uint8_t)(channel_pwm.A.pwmindex)) * 91);
                        TimerPwmABrakeThenFloat.number_of_delays = 0; //no timeout
                        break;
                    }
                    case OM_BRAKE_THEN_FLOAT:
                        TimerPwmABrakeThenFloat.number_of_delays = 10;//1s braking
                    case OM_BRAKE:
                    {
                        TIM_SetCompare1(TIM1,8*91);
                        TIM_SetCompare2(TIM1,8*91);
                        break;
                    }
                    default:
                    {
                        TIM_SetCompare1(TIM1,0);
                        TIM_SetCompare2(TIM1,0);
                        TimerPwmABrakeThenFloat.number_of_delays = 0; //no timeout
                    }
                    }
                    switch(channel_pwm.B.output_mode)
                    {
                    case OM_FWD:
                    {
                        TIM_SetCompare3(TIM1,((uint8_t)(channel_pwm.B.pwmindex)) * 91);
                        TIM_SetCompare4(TIM1,0);
                        TimerPwmABrakeThenFloat.number_of_delays = 0; //no timeout
                        break;
                    }
                    case OM_BWD:
                    {
                        TIM_SetCompare3(TIM1,0);
                        TIM_SetCompare4(TIM1,((uint8_t)(channel_pwm.B.pwmindex)) * 91);
                        TimerPwmABrakeThenFloat.number_of_delays = 0; //no timeout
                        break;
                    }
                    case OM_BRAKE_THEN_FLOAT:
                        TimerPwmBBrakeThenFloat.number_of_delays = 10;//1s braking
                    case OM_BRAKE:
                    {
                        TIM_SetCompare3(TIM1,8*91);
                        TIM_SetCompare4(TIM1,8*91);
                        TimerPwmABrakeThenFloat.number_of_delays = 0; //no timeout
                        break;
                    }
                    default:
                    {
                        TIM_SetCompare3(TIM1,0);
                        TIM_SetCompare4(TIM1,0);
                    }
                    }
                }
                if(channelnumber == channel_servo.channel_number)
                {
                    channel_servo.timeout = channel_servo.timeout_limit;                       //reset timeout
                    OpenPfRxInterpreter((const uint16_t *)&legochannel[channelnumber] , &channel_servo);

                    if(channel_servo.A.output_mode == OM_FWD || channel_servo.A.output_mode == OM_BWD)
                    {
                        int8_t temp;
                        temp = (int8_t)channel_servo.A.pwmindex;
                        if(channel_servo.A.output_mode == OM_BWD)
                            temp = -temp;
                        TIM_SetCompare1(TIM3,(SERVO_OFFSET + temp)*SERVO_MULTIPLIER);
                    }
                    else
                    {
                        TIM_SetCompare1(TIM3,(SERVO_OFFSET)*SERVO_MULTIPLIER);
                    }
                    if(channel_servo.B.output_mode == OM_FWD || channel_servo.B.output_mode == OM_BWD)
                    {
                        int8_t temp;
                        temp = (int8_t)channel_servo.B.pwmindex;
                        if(channel_servo.B.output_mode == OM_BWD)
                            temp = -temp;
                        TIM_SetCompare2(TIM3,(SERVO_OFFSET + temp)*SERVO_MULTIPLIER);
                    }
                    else
                    {
                        TIM_SetCompare2(TIM3, SERVO_OFFSET *SERVO_MULTIPLIER);
                    }
                }
            }
        }
        if(externalint) //check if external interrupt has taken place
        {
            externalint = 0; //clear flag
            OpenPfRxPinInterruptState(); //process counters / pin state to gather IR data
            TimerSleepTimeOut.number_of_delays = 10; //wati a second before going to sleep
        }
    }
}

static uint8_t HandleButton(uint8_t b_value)
{
    uint8_t action = BTN_DO_NOTHING;
    static uint8_t pushcounter = 0;
   if(b_value)
    {
        if(pushcounter < 20)
            pushcounter++;
    }
    else
    {
        if(pushcounter >= 10) //longer than 800ms
            action = BTN_LONG_PUSH;
        else if (pushcounter >= 2) //longer than 200ms
            action = BTN_SHORT_PUSH;
        pushcounter = 0;
    }
    return action;
}

static void UpdateOutputValues(struct OpenPfRx_output * output)
{
    switch(output->output_mode)
    {
    case OM_FWD:
    {
        if(output->pwmvalue != OpenPfRx_MIN_PWM_VALUE)
        {
            output->C1 = 1;
            output->C2 = 0;
        }
        else
        {
            //FLOAT
            output->C1 = 0;
            output->C2 = 0;
        }
        break;
    }
    case OM_BWD:
    {
        if(output->pwmvalue != OpenPfRx_MIN_PWM_VALUE)
        {
            output->C1 = 0;
            output->C2 = 1;
        }
        else
        {
            //FLOAT
            output->C1 = 0;
            output->C2 = 0;
        }
        break;
    }
    case OM_FLOAT:
    {
        output->C1 = 0;
        output->C2 = 0;
        break;
    }
    case OM_BRAKE_THEN_FLOAT:
        output->brakethenfloatcount= 0;
    case OM_BRAKE:
    {
        output->C1 = 1;
        output->C2 = 1;
        break;
    }
    case OM_INDEPENDENT:
    default:
        break;
    }
}

static void DecreaseTimers(void)
{
    DecreaseTimer(&TimerPwmABrakeThenFloat);
    DecreaseTimer(&TimerPwmBBrakeThenFloat);
    DecreaseTimer(&TimerServoPfTimeOut);
    DecreaseTimer(&TimerPwmPfTimeOut);
    DecreaseTimer(&TimerSleepTimeOut);
}

static void DecreaseTimer(TimerStructTypeDef * timer)
{
    if(timer->number_of_delays > 0)
    {
        timer->number_of_delays--;
        if(timer->number_of_delays == 0)
            timer->flag = SET;
    }
}

void delay (int32_t a)
{
    volatile int i,j;
    for (i=0 ; i < a ; i++)
    {
        j++;
    }

    return;
}
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
