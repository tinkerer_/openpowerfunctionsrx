//Copyright (c) 2012, vsluiter <info-at- hackvandedam.nl>
//
//Permission to use, copy, modify, and/or distribute this software for any
//purpose with or without fee is hereby granted, provided that the above
//copyright notice and this permission notice appear in all copies.
//
//THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
//WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
//AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
//INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
//OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
//TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
//OF THIS SOFTWARE.


#ifndef HAL_H_
#define HAL_H_

#include <avr/io.h>
#define ExternalInterruptLowLevel  0b00
#define ExternalInterruptHighLevel 0b01
#define ExternalInterruptFalling   0b10
#define ExternalInterruptRising    0b11

uint8_t test;

void SetupExternalInterrupt(uint8_t mode);
void Setup105usclock(); //105us clock
void IoInit(void);
void SetupPWMTimer(void);
uint8_t ReadChannel(void);
#define PWMCLK _BV(CS10)
#define START_PWM_TIMER TCCR1B |=  PWMCLK
#define RESET_PWM_TIMER TCNT1 = 1;
#define STOP_PWM_TIMER TCCR1B  &=  0xF8;
#define DISABLE_IR_INT   GIMSK &= ~(_BV(INT0))
#define ENABLE_IR_INT    GIMSK |= _BV(INT0)
//Clear Timer, restart counting
#define RESET_IR_TIMER    TCNT0 = 0
#define TIMER_105US       TIM0_COMPA_vect
#define EXTERNAL_INTERRUPT EXT_INT0_vect
#define PWMTIMER_PERIODSTART TIM1_CAPT_vect
#define PWMTIMER_PWMA_INTERRUPT TIM1_COMPA_vect
#define PWMTIMER_PWMB_INTERRUPT TIM1_COMPB_vect
#define DISABLE_PWMA_INTERRUPT TIMSK1 &= ~(_BV(OCIE1A))
#define ENABLE_PWMA_INTERRUPT  TIMSK1 |=   _BV(OCIE1A)
#define DISABLE_PWMB_INTERRUPT TIMSK1 &= ~(_BV(OCIE1B))
#define ENABLE_PWMB_INTERRUPT  TIMSK1 |=   _BV(OCIE1B)
#define A_PORT PORTA
#define B_PORT PORTA
#define A_PIN PINA
#define B_PIN PINB
#define A_C1 0x01
#define A_C2 0x02
#define B_C1 0x04
#define B_C2 0x08
#define A_C1_LOW (A_PORT &= ~(A_C1))
#define A_TOGGLE A_PIN = A_C1|A_C2
#define A_C2_LOW (A_PORT &= ~(A_C2))
#define B_C1_LOW (B_PORT &= ~(B_C1))
#define B_C2_LOW (B_PORT &= ~(B_C2))
#define B_TOGGLE B_PIN = B_C1|B_C2
#define SERVOAPIN 0x20
#define SERVOAPORT PORTA
#define SERVOADDR  DDRA
#define SERVOAPINHI SERVOAPORT |=  SERVOAPIN
#define SERVOAPINLO SERVOAPORT &= ~SERVOAPIN
#define SERVOBPIN 0x40
#define SERVOBPORT PORTA
#define SERVOBDDR  DDRA
#define SERVOBPINHI SERVOBPORT |=  SERVOBPIN
#define SERVOBPINLO SERVOBPORT &= ~SERVOBPIN
#define BUTTON_PORT PORTA
#define BUTTON_PIN  PINA
#define BUTTON_DDR  DDRA
#define BUTTONPINMASK   0x80
#define BUTTON_PUSHED ((~BUTTON_PIN) & BUTTONPINMASK)
#define BICOLOR_LED_OUTPUTS DDRB |= 0x3;
#define BICOLOR_RED   PORTB = (PORTB | 0x03) & ~0x01
#define BICOLOR_GREEN PORTB = (PORTB | 0x03) & ~0x02
#endif
