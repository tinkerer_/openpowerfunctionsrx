//Copyright (c) 2012, vsluiter <info-at- hackvandedam.nl>
//
//Permission to use, copy, modify, and/or distribute this software for any
//purpose with or without fee is hereby granted, provided that the above
//copyright notice and this permission notice appear in all copies.
//
//THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
//WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
//AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
//INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
//OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
//TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
//OF THIS SOFTWARE.

#include <avr/io.h>
#include <avr/interrupt.h>
#include "hal.h"
#define _NOP() asm("nop")
void SetupExternalInterrupt(uint8_t mode)
{
    /*Init pin interrupt*/
    MCUCR = (MCUCR & 0xFC) | mode; //Falling edge INT0 generates interrupt; Table 9-2
}

void Setup105usclock(void)
{
    /*Timer0 CTC mode, overrun at OCR0A, CLK/8*/
    TCCR0A = _BV(WGM01);
    TCCR0B = _BV(CS01);        // CTC mode, TOP is OCR0A, CLK = CLKio/8
    TCNT0  = 0;
    OCR0A  = 105; //105us
    TIMSK0 |= _BV(OCIE0A); //Output compare interrupt enabled
}

void IoInit(void)
{
    DDRA = 0;
    DDRB = 0;
    PORTA = 0x00;
    PORTB = 0xFF;
    A_PORT  |= (A_C1|A_C2|B_C1|B_C2);
    DDRA    |= (A_C1|A_C2|B_C1|B_C2);
    SERVOADDR |= SERVOAPIN;       //make servoa control pin output
    SERVOBDDR |= SERVOBPIN;       //make servob control pin output
    BUTTON_DDR  &= ~BUTTONPINMASK;//pullup on button pin
    BUTTON_PORT |=  BUTTONPINMASK;//pullup on button pin
    BICOLOR_LED_OUTPUTS;
}

void SetupPWMTimer(void)
{
    TCCR1A =0; //CTC
    TCCR1B = (_BV(WGM13) | _BV(WGM12) | PWMCLK);                 //IOclk/1 ->No clock division
    TCCR1C = 0;
    ICR1   = 511; //2x 255 -> 16kHz output frequency
    OCR1A  = 20;  //Value to turn off output A
    OCR1B  = 20;  //Value to turn off output B
    TCNT1  = 0;
    TIMSK1 = _BV(ICIE1);
}
